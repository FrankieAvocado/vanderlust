class JourneyStop {
  String id;
  String image;
  String country;
  String place;
  String coordinates;
  String adventure;

  JourneyStop({this.id, this.image, this.country, this.place, this.coordinates, this.adventure});

  factory JourneyStop.fromJson(Map<String, dynamic> json) {
    return JourneyStop(
      id: json['id'],
      image: json['image'],
      country: json['country'],
      place: json['place'],
      coordinates: json['coordinates'],
      adventure: json['adventure']
    );
  }

  Map toJson() {
    Map map = new Map();
    map["id"] = id;
    map["image"] = image;
    map["country"] = country;
    map["place"] = place;
    map["coordinates"] = coordinates;
    map["adventure"] = adventure;
    return map;
  }

}

class JourneyList {
  List<JourneyStop> stops;

  JourneyList({this.stops});

  factory JourneyList.fromJson(Map<String, dynamic> json) {
    var allStops = (json['stops'] as List).map((stop) => new JourneyStop.fromJson(stop));
    return JourneyList(
      stops: allStops
    );
  }

  Map toJson() {
    Map map = new Map();
    map["stops"] = stops;
    return map;
  }
}